var jwt = require('jsonwebtoken');
require('dotenv').config()


function verifyUserAccessToken(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token)
    return res.status(403).send({ auth: false, message: 'No token provided.' });

  jwt.verify(token, process.env.TOKENSECRET, function (err, decoded) {
    if (err)
      return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    // verificamos si el usuario que pide tiene acceso a este recurso
    if (req.params.id != decoded.id) {
      return res.status(403).send({ auth: false, message: 'You do not have permission to access to this resource.' });
    };
    next();
  });
}

module.exports = verifyUserAccessToken;