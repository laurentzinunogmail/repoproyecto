var mongoose = require('mongoose');
var AccountSchema = new mongoose.Schema({

});

var AccountSchema = new mongoose.Schema({
  iban: String,
  creation_date: { type: Date, default: Date.now },
  balance: Number,
  movements: [{
    creation_date: { type: Date, default: Date.now },
    origin: String,
    destiny: String,
    amount_movement: Number,
    account_balance: Number
  }],
}, {
    versionKey: false
  });
mongoose.model('Account', AccountSchema);

module.exports = mongoose.model('Account');