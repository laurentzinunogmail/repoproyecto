var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Account = require('./Account');

// CREATES A NEW ACCOUNT FOR AN USER

router.post('/', function (req, res) {
    Account.create({
        iban: req.body.iban,
        balance: 0
    },
        function (err, account) {
            if (err) return res.status(500).send("There was a problem adding the information -account- to the database.");
            res.status(200).send(account);
        });
});

// RETURNS ALL THE ACCOUNTS IN THE DATABASE
router.get('/', function (req, res) {
    Account.find({}, function (err, accounts) {
        if (err) return res.status(500).send("There was a problem finding the accounts.");
        res.status(200).send(accounts);
    });
});

// GETS A SINGLE ACCOUNT FROM THE DATABASE
router.get('/:id', function (req, res) {
    Account.findById(req.params.id, function (err, account) {
        if (err) return res.status(500).send("There was a problem finding the account.");
        if (!account) return res.status(404).send("No account found.");
        res.status(200).send(account);
    });
});

// DELETES A ACCOUNT FROM THE DATABASE
router.delete('/:id', function (req, res) {
    Account.findByIdAndRemove(req.params.id, function (err, account) {
        if (err) return res.status(500).send("There was a problem deleting the account.");
        res.status(200).send("Account: " + account.iban + " was deleted.");
    });
});

// UPDATES A SINGLE ACCOUNT IN THE DATABASE
router.put('/:id', function (req, res) {
    // Account.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, account) {
    //     if (err) return res.status(500).send("There was a problem updating the account.");
    //     res.status(200).send(account);
    // });
    var newMovement = {
        origin: "origin",
        destiny: "destiny",
        amount_movement: 1,
        account_balance: 1
    }
    Account.update(
        { _id: req.params.id },
        { $push: { movements: newMovement } },
        function (err, account) {
            if (err) return res.status(500).send("There was a problem updating the account.");
            res.status(200).send(account);
        }
    );
});



module.exports = router;