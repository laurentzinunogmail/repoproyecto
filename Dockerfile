#Imagen raiz
FROM node

# Carpeta raiz o directorio de trabajo
WORKDIR /apitechu

# Copia de archivos de carpeta local a  apitechu
# Esto se suele hacer un git clone para no llevarse las dependencias
ADD . /apitechu

# procseo de despliegue, instalamos node_modules
RUN npm install

#Puerto que expone
EXPOSE 3000

# Comando de inicialización, se ejecutará al ejecutar el RUN (cuando se construye la imagen)
# los comandos RUN se ejecutan en el BUILD
CMD ["npm", "start"]

# Vamos a ignorar en el archivo .dockerignore
# .git --> no subir el repositorio
# node_modules --> lo isntalaremos con npm install
# .env --> variables de entorno, aunque en nuestro caso

