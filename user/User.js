var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({  
  first_name: String,
  last_name: String,
  email: String,
  password: String,
  creation_date: {type: Date, default: Date.now}
}, {
  versionKey: false 
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');